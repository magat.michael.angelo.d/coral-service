module.exports = function(app, axios){
    //app.get('/payment-rawdah-cc', func);
    app.get('/ottu', func);
  
    function func(request, response, next) {
        var params = request.query;

        var base_url = 'https://pay.coralschool.edu.sa/pos/crt/';

        var disclosure_url = "";
        var redirect_url = "";

        switch(params.type) {
            case 'exam':
                disclosure_url = "https://coralschool.edu.sa/payment-exam-disclosed/"+params.order_no+"/exam";
                redirect_url = "https://coralschool.edu.sa/payment-exam-success/"+params.order_no+"/exam";
                break;
            case 'books':
                disclosure_url = "https://coralschool.edu.sa/student/books-disclosed/"+params.order_no+"/books";
                redirect_url = "https://coralschool.edu.sa/student/books-success/"+params.order_no+"/books";
                break;
            case 'reserve_fee':
                disclosure_url = "https://coralschool.edu.sa/student/reserve-disclosed/"+params.order_no+"/reserve_fee";
                redirect_url = "https://coralschool.edu.sa/student/reserve-success/"+params.order_no+"/reserve_fee";
                break;
            case 'tuition_fee':
                disclosure_url = "https://coralschool.edu.sa/student/tuition-disclosed/"+params.order_no+"/tuition_fee";
                redirect_url = "https://coralschool.edu.sa/student/tuition-success/"+params.order_no+"/tuition_fee";
                break;
            case 'uniform':
                disclosure_url = "https://coralschool.edu.sa/student/uniforms-disclosed/"+params.order_no+"/uniform";
                redirect_url = "https://coralschool.edu.sa/student/uniforms-success/"+params.order_no+"/uniform";
                break;
        }

        axios.post(base_url, {
            "amount": params.amount,
            "currency_code": "SAR",
            //"gateway_code": "credit-card-test",
            "gateway_code": "credit-card",
            "order_no": params.order_no,
            "disclosure_url": disclosure_url,
            "redirect_url": redirect_url,
            "email_payment_details": true,
            "customer_email": params.customer_email,
            "customer_first_name": params.customer_first_name,
            "customer_last_name": params.customer_last_name,
            "customer_address_line1": params.customer_address_line1,
            "customer_address_city": params.customer_address_city,
            "customer_address_postal_code": params.customer_address_postal_code,
            "extra": {
                student_id: params.student_id,
                student_name: params.student_name,
                grade: params.grade,
                gender: params.gender,
                school_branch: params.school_branch
            }
        }, {
            headers: {
                "Content-Type": "application/json"
            }
        }).then((res) => {
            response.send({
                status: 'ok',
                url: res.data.url
            });
        })
        .catch((error) => {
            response.send(error)
        });
    }
  }