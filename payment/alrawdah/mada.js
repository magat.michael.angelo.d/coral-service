module.exports = function(app, https, querystring, base_url){
  
  //
  // Entity ID
  //
  //var liveAuthToken = 'OGFjN2E0Yzc3NjgzZGM1ZDAxNzY4NTc4MzI5NDA0ZDd8UktUeVczeG42Vw==';
  var liveAuthToken = 'OGFjZGE0Y2Q3NzFmMDhiMTAxNzcyMTBkZmIzMTI3MWF8eWVEcmIzWFNXbg==';

  //
  // Entity ID
  //
  //var liveEntityID = '8ac7a4c77683dc5d0176857953bd04e0';
  var liveEntityID = '8acda4cd771f08b10177211043fe2733';

  //app.get('/payment-rawdah-mada', func);
  app.get('/payment-murjan-mada', func);

  async function func(request, response, next) {
    payment(request).then(data => {
      response.send(data);
    }).catch(console.error);
  }

  var payment = async function(req) {

    const path='/v1/checkouts';
    const data = querystring.stringify({
      'entityId': liveEntityID,
      'amount': req.query.amount,
      'currency':'SAR',
      'paymentType':'DB',
      'merchantTransactionId': req.query.merchantTransactionId,
      'customer.email': req.query.email,
      'customer.givenName': req.query.givenName,
      'customer.surname': req.query.surname,
      'billing.street1': req.query.street1,
      'billing.city': req.query.city,
      'billing.state': req.query.state,
      'billing.postcode': req.query.postcode,
      'billing.country': 'SA',
    });
    const options = {
      port: 443,
      host: base_url,
      path: path,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': data.length,
        'Authorization':'Bearer '+liveAuthToken,
      }
    };

    return new Promise((resolve, reject) => {
      const postRequest = https.request(options, function(res) {
        const buf = [];
        res.on('data', chunk => {
          buf.push(Buffer.from(chunk));
        });
        res.on('end', () => {
          const jsonString = Buffer.concat(buf).toString('utf8');
          try {
            resolve(JSON.parse(jsonString));
          } catch (error) {
            reject(error);
          }
        });
      });
      postRequest.on('error', reject);
      postRequest.write(data);
      postRequest.end();
    });
  }
}