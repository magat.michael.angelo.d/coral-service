module.exports = function(app, axios){

  app.get('/email-registration', func);

  function func(req, res, next) {

    if(Object.keys(req.query).length === 0) {
      res.send('No Data submitted');
    }else {

      var data = req.query;

      axios.get('http://services.coralschool.edu.sa/email_register.php', {
        params: {
          fullname : data.fullname,
          date_of_birth : data.date_of_birth,
          place_of_birth : data.place_of_birth,
          school_branch : data.school_branch,
          grade : data.grade,
          semester : data.semester,
          school_attend: data.school_attend,
          gender : data.gender,
          nationality : data.nationality,
          religion : data.religion,
          national_iqama_id : data.national_iqama_id,
          national_iqama_expiry : data.national_iqama_expiry,
          passport_no : data.passport_no,
          passport_expiry : data.passport_expiry,
          name_of_last_school : data.name_of_last_school,
          fathers_fullname: data.fathers_fullname,
          fathers_nationality: data.fathers_nationality,
          fathers_job: data.fathers_job,
          fathers_workplace: data.fathers_workplace,
          fathers_mobile: data.fathers_mobile,
          fathers_email: data.fathers_email,
          fathers_home_tel: data.fathers_home_tel,
          fathers_id: data.fathers_id,
          fathers_id_expiry: data.fathers_id_expiry,
          mothers_fullname: data.mothers_fullname,
          mothers_nationality: data.mothers_nationality,
          mothers_job: data.mothers_job,
          mothers_workplace: data.mothers_workplace,
          mothers_mobile: data.mothers_mobile,
          mothers_email: data.mothers_email,
          mothers_home_tel: data.mothers_home_tel,
          mothers_id: data.mothers_id,
          mothers_id_expiry: data.mothers_id_expiry,
          emergency_contact: data.emergency_contact,
          national_address: data.national_address,
          region: data.region,
          city: data.city,
          district: data.district,
          street_name: data.street_name,
          bldg_no: data.bldg_no,
          appartment_no: data.appartment_no,
          zip_code: data.zip_code,
          additional_no: data.additional_no
        }
      }).then((res) => {
        res.send('success');
      })
      .catch((error) => {
        res.send(error)
      });
    }
  }
}