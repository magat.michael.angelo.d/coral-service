module.exports = function(app, axios){

  app.get('/email-parents-accepted', func);

  function func(req, res, next) {

    if(Object.keys(req.query).length === 0) {
      res.send('No Data submitted');
    }else {

      var data = req.query;

      axios.get('http://services.coralschool.edu.sa/email_parents_accepted.php', {
        params: {
          fathers_email: data.fathers_email,
          mothers_email: data.mothers_email,
          fullname: data.fullname,
          national_iqama_id: data.national_iqama_id,
          school_branch: data.school_branch,
          grade: data.grade,
          gender: data.gender,
        }
      }).then((res) => {
        res.send('success');
      })
      .catch((error) => {
        res.send(error)
      });
    }
  }
}