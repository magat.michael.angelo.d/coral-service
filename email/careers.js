module.exports = function(app, axios){

  app.get('/email-careers', func);

  function func(req, res, next) {

    if(Object.keys(req.query).length === 0) {
      res.send('No Data submitted');
    }else {

      var data = req.query;

      axios.get('http://services.coralschool.edu.sa/email_careers.php', {
        params: {
          first_name: data.first_name,
          last_name: data.last_name,
          position: data.position,
          email: data.email,
          contact_no: data.contact_no,
          nationality: data.nationality,
          country: data.country,
          address: data.address,
          resume_cv: data.resume_cv,
          cover_letter: data.cover_letter,
          additional_info: data.additional_info
        }
      }).then((res) => {
        res.send('success');
      })
      .catch((error) => {
        res.send(error)
      });
    }
  }
}