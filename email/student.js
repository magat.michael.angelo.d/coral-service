module.exports = function(app, axios){

  app.get('/email-student', func);

  function func(req, res, next) {

    if(Object.keys(req.query).length === 0) {
      res.send('No Data submitted');
    }else {

      var data = req.query;

      axios.get('http://services.coralschool.edu.sa/email_student.php', {
        params: {
          email: data.email,
          fathers_email: data.fathers_email,
          mothers_email: data.mothers_email,
          national_iqama_id: data.national_iqama_id,
          student_name: data.student_name,
          grade: data.grade,
          school_branch: data.school_branch,
          status: data.status,
          subject: data.subject
        }
      }).then((res) => {
        res.send('success');
      })
      .catch((error) => {
        res.send(error)
      });
    }
  }
}