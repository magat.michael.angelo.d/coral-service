module.exports = function(app, axios){

  app.get('/email-sendpassword', func);

  function func(req, res, next) {

    if(Object.keys(req.query).length === 0) {
      res.send('No Data submitted');
    }else {

      var data = req.query;

      axios.get('http://services.coralschool.edu.sa/email_sendpassword.php', {
        params: {
          national_iqama_id: data.national_iqama_id,
          student_password: data.student_password,
          email_father: data.email_father,
          email_mother: data.email_mother,
          email: data.email,
        }
      }).then((res) => {
        res.send('success');
      })
      .catch((error) => {
        res.send(error)
      });
    }
  }
}