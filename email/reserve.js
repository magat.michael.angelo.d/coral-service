module.exports = function(app, axios){

  app.get('/email-reserve', func);

  function func(req, res, next) {

    if(Object.keys(req.query).length === 0) {
      res.send('No Data submitted');
    }else {

      var data = req.query;

      axios.get('http://services.coralschool.edu.sa/email_reserve.php', {
        params: {
          email: data.email,
          givenName: data.givenName,
          surname: data.surname,
          street1: data.street1,
          city: data.city,
          state: data.state,
          postcode: data.postcode,
          school_branch: data.school_branch,
          transaction_id: data.transaction_id,
          checkout: data.checkout,
          year: data.year,
          national_iqama_id: data.national_iqama_id,
          student_name: data.student_name,
          payment_type: data.payment_type,
          payment_id: data.payment_id,
          date_added: data.date_added,
          type: data.type,
          amount: data.amount,
          overall: data.overall,
          vat: data.vat,
          currency: data.currency,
          status: data.status,
          gender: data.gender,
          grade: data.grade
        }
      }).then((res) => {
        res.send('success');
      })
      .catch((error) => {
        res.send(error)
      });
    }
  }
}