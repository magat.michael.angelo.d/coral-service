var express = require('express');
var cors = require('cors')
var app = express();
var nodemailer = require('nodemailer');
var axios = require('axios');
var https = require('https');
var querystring = require('querystring');

//var base_url = 'test.oppwa.com';
var base_url = 'oppwa.com';

app.use(cors());

app.get('/', function (req, res) {
  res.send('Access Denied');
});

//
// Payment API
//
require('./payment/ottu/payment')(app, axios);
require('./payment/alrawdah/cc')(app, https, querystring, base_url);
require('./payment/alrawdah/mada')(app, https, querystring, base_url);
require('./payment/almurjan/cc')(app, https, querystring, base_url);
require('./payment/almurjan/mada')(app, https, querystring, base_url);
//
// Email API
//
require('./email/registration')(app, axios);
require('./email/books')(app, axios);
require('./email/exam')(app, axios);
require('./email/tuition')(app, axios);
require('./email/uniforms')(app, axios);
require('./email/careers')(app, axios);
require('./email/reserve')(app, axios);
require('./email/student')(app, axios);
require('./email/sendpassword')(app, axios);
require('./email/parentsaccepted')(app, axios);

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log("Server running on port 5000");
});